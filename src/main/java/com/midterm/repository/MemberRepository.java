package com.midterm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.midterm.entity.Member;


public interface MemberRepository extends JpaRepository<Member, Long> {
	List<Member> findByFemale(boolean female);
}
